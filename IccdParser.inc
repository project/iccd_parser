<?php

/**
 * @file
 * Implementation of FeedsParser::parse().
 *
 */

/**
 * Class definition for Common Syndication Parser.
 *
 * Parses RSS and Atom feeds.
 */
class IccdParser extends FeedsParser {

  /**
   * Implementation of FeedsParser::parse().
   */
  public function parse(FeedsImportBatch $batch, FeedsSource $source) {
    #feeds_include_library('bclt_parser.inc', 'bclt');
    require(drupal_get_path('module', 'iccd_parser') . '/iccd_parser.inc');
    $result = iccd_parser_parse($batch->getRaw());
    $batch->setTitle($result['title']);
    $batch->setDescription($result['description']);
    $batch->setLink($result['link']);
    if (is_array($result['items'])) {
      $batch->setItems($result['items']);
    }
  }

  /**
   * Return mapping sources.
   *
   * At a future point, we could expose data type information here,
   * storage systems like Data module could use this information to store
   * parsed data automatically in fields with a correct field type.
   */
  public function getMappingSources() {
    return array(
      'title' => array(
        'name' => t('Title (code: RSSN)'),
        'description' => t('Name of the Site.'),
      ),
      'guid' => array(
        'name' => t('ID (code: ISK)'),
        'description' => t('Entry Code'),
      ),
      'description' => array(
        'name' => t('Description (code: DEGG)'),
        'description' => t('Park or Archelogical Area Description.'),
      ),
      'timestamp' => array(
        'name' => t('Publication Date (code: ETS)'),
        'description' => t('Publication Date'),
      ),
      'daci' => array(
        'name' => t('Admission Terms (code: DACI)'),
        'description' => t('Admission Terms')
      ),
      'esc' => array(
        'name' => t('Filing Authority (code: ESC)'),
        'description' => t('Filing Authority')
      ),
      'orgt' => array(
        'name' => t('Structure\'s type (code: ORGT)'),
        'description' => t('Structure\'s type')
      ),
      'orgn' => array(
        'name' => t('Structure Denomination (code: ORGN)'),
        'description' => t('Structure Denomination')
      ),
      'orga' => array(
        'name' => t('Istitutive Record (code: ORGA)'),
        'description' => t('Istitutive Record')
      ),
      'orgi' => array(
        'name' => t('Istitutive Record Date (code: ORGI)'),
        'description' => t('Istitutive Record Date')
      ),
      'orpd' => array(
        'name' => t('Owning Authority Denomination (code: ORPD)'),
        'description' => t('Owning Authority Denomination')
      ),
      'orpt' => array(
        'name' => t('Property Type (code: ORPT)'),
        'description' => t('Property Type')
      ),
      'orpq' => array(
        'name' => t('Type of Typology (code: ORPQ)'),
        'description' => t('Type of Typology')
      ),
      'orpn' => array(
        'name' => t('Owning Authority Notes (code: ORPN)'),
        'description' => t('Owning Authority Notes')
      ),
      'ored' => array(
        'name' => t('Managing Authority Denomination (code: ORED)'),
        'description' => t('Managing Authority Denomination')
      ),
      'oret' => array(
        'name' => t('Management Type (code: ORET)'),
        'description' => t('Management Type')
      ),
      'oreq' => array(
        'name' => t('Management Typlogy Type (code: OREQ)'),
        'description' => t('Management Typlogy Type')
      ),
      'oref' => array(
        'name' => t('Telephone (code: OREF)'),
        'description' => t('Telephone')
      ),
      'oren' => array(
        'name' => t('Managing Authority Notes (code: OREN)'),
        'description' => t('Managing Authority Notes')
      ),
      'degg' => array(
        'name' => t('Park or Archelogical Area Description (code: DEGG)'),
        'description' => t('Park or Archelogical Area Description')
      ),
      'degs' => array(
        'name' => t('Security System Description (code: DEGS)'),
        'description' => t('Security System Description')
      ),
      'degv' => array(
        'name' => t('Way of Access Description (code: DEGV)'),
        'description' => t('Way of Access Description')
      ),
      'degz' => array(
        'name' => t('In Zone Signs (code: DEGZ)'),
        'description' => t('In Zone Signs')
      ),
      'dega' => array(
        'name' => t('Antiquarium (code: DEGA)'),
        'description' => t('Antiquarium')
      ),
      'deaa' => array(
        'name' => t('Organization Description (code: DEAA)'),
        'description' => t('Organization Description')
      ),
      'deac' => array(
        'name' => t('Systems of Comunication Description (code: DEAC)'),
        'description' => t('Systems of Comunication Description')
      ),
      'deas' => array(
        'name' => t('Category Related Description (code: DEAS)'),
        'description' => t('Category Related Description')
      ),
      'dacn' => array(
        'name' => t('Admission Term Notes (code: DACN)'),
        'description' => t('Admission Term Notes')
      ),
      'dacp' => array(
        'name' => t('In Zone Parking (code: DACP)'),
        'description' => t('In Zone Parking')
      ),
      'daca' => array(
        'name' => t('Removal of architectural feature that denies access to the handicapped (code: DACA)'),
        'description' => t('Removal of architectural feature that denies access to the handicapped')
      ),
      'dacd' => array(
        'name' => t('Minimum Tour Length (code: DACD)'),
        'description' => t('Minimum Tour Length')
      ),
      'dacf' => array(
        'name' => t('Maximum Tour Length (code: DACF)'),
        'description' => t('Maximum Tour Length')
      ),
      'dacv' => array(
        'name' => t('Tour Path Conditions (code: DACV)'),
        'description' => t('Tour Path Conditions')
      ),
      'dacm' => array(
        'name' => t('Walkable Path (code: DACM)'),
        'description' => t('Walkable Path')
      ),
      'oapa' => array(
        'name' => t('Opening Day (code: OAPA)'),
        'description' => t('Opening Day')
      ),
      'oapo' => array(
        'name' => t('Opening Time (code: OAPO)'),
        'description' => t('Opening Time')
      ),
      'oapn' => array(
        'name' => t('Opening Notes (code: OAPN)'),
        'description' => t('Opening Notes')
      ),
      'oapc' => array(
        'name' => t('Closing Day (code: OAPC)'),
        'description' => t('Closing Day')
      ),
      'oapm' => array(
        'name' => t('Reservation Conditions (code: OAPM)'),
        'description' => t('Reservation Conditions')
      ),
      'dspb' => array(
        'name' => t('Bookshop Description (code: DSPB)'),
        'description' => t('Bookshop Description')
      ),
      'dspr' => array(
        'name' => t('Restaurant Description (code: DSPR)'),
        'description' => t('Restaurant Description')
      ),
      'dspa' => array(
        'name' => t('Pic-nic area Description (code: DSPA)'),
        'description' => t('Pic-nic area Description')
      ),
      'dsps' => array(
        'name' => t('Toilet Facilities Description (code: DSPS)'),
        'description' => t('Toilet Facilities Description')
      ),
      'dsdv' => array(
        'name' => t('Guided Tour (code: DSDV)'),
        'description' => t('Guided Tour')
      ),
      'ogtd' => array(
        'name' => t('Definition (code: OGTD)'),
        'description' => t('Definition')
      ),
      'ogtt' => array(
        'name' => t('Typology Specification (code: OGTT)'),
        'description' => t('Typology Specification')
      ),
      'ogtc' => array(
        'name' => t('Category Affiliation (code: OGTC)'),
        'description' => t('Category Affiliation')
      ),
      'ogty' => array(
        'name' => t('Traditional or Historical Denomination (code: OGTY)'),
        'description' => t('Traditional or Historical Denomination')
      ),
      'ogtf' => array(
        'name' => t('Function (code: OGTF)'),
        'description' => t('Function')
      ),
      'ogtn' => array(
        'name' => t('Denomination (code: OGTN)'),
        'description' => t('Denomination')
      ),
      'pvcs' => array(
        'name' => t('State (code: PVCS)'),
        'description' => t('State')
      ),
      'pvcr' => array(
        'name' => t('Region (code: PVCR)'),
        'description' => t('Region')
      ),
      'pvcp' => array(
        'name' => t('Province (code: PVCP)'),
        'description' => t('Province')
      ),
      'pvcc' => array(
        'name' => t('City (code: PVCC)'),
        'description' => t('City')
      ),
      'pvcl' => array(
        'name' => t('Locality (code: PVCL)'),
        'description' => t('Locality')
      ),
      'pvci' => array(
        'name' => t('Address (code: PVCI)'),
        'description' => t('Address')
      ),
      'dtzg' => array(
        'name' => t('Chronological Reference (code: DTZG)'),
        'description' => t('Chronological Reference')
      ),
      'dtsi' => array(
        'name' => t('Date (code: DTSI)'),
        'description' => t('Date')
      ),
      'dtm' => array(
        'name' => t('Chronological Reason (code: DTM)'),
        'description' => t('Chronological Reason')
      ),
      'stcp' => array(
        'name' => t('Preservation Part Reference (code: STCP)'),
        'description' => t('Reference')
      ),
      'stcc' => array(
        'name' => t('Preservation Status (code: STCC)'),
        'description' => t('Preservation Status')
      ),
      'rstp' => array(
        'name' => t('Restauration Part Reference (code: RSTP)'),
        'description' => t('Restauration Part Reference')
      ),
      'rstt' => array(
        'name' => t('Restauration Activity (code: RSTT)'),
        'description' => t('Restauration Activity')
      ),
      'deso' => array(
        'name' => t('Description (code: DESO)'),
        'description' => t('Description')
      ),
      'nsc' => array(
        'name' => t('Critical Historical Informations (code: NSC)'),
        'description' => t('Critical Historical Informations')
      ),
      'fnsd' => array(
        'name' => t('Foundation Descriptions (code: FNSD)'),
        'description' => t('Foundation Descriptions')
      ),
      'eled' => array(
        'name' => t('Indoor Descriptions (code: ELED)'),
        'description' => t('Indoor Descriptions')
      ),
      'spad' => array(
        'name' => t('Open Space Descriptions (code: SPAD)'),
        'description' => t('Open Space Descriptions')
      ),
      'cdgg' => array(
        'name' => t('General Indications (code: CDGG)'),
        'description' => t('General Indications')
      ),
      'cdgs' => array(
        'name' => t('Specific Indication (code: CDGS)'),
        'description' => t('Specific Indication')
      ),
      'cdgi' => array(
        'name' => t('Address (code: CDGI)'),
        'description' => t('Address')
      ),
      'nvct' => array(
        'name' => t('Type of Measure (code: NVCT)'),
        'description' => t('Type of Measure')
      ),
      'nvce' => array(
        'name' => t('Measure Details (code: NVCE)'),
        'description' => t('Measure Details')
      ),
      'fta' => array(
        'name' => t('Photografic Documentation (code: FTA)'),
        'description' => t('Photografic Documentation')
      ),
      'dra' => array(
        'name' => t('Graphic Documentation (code: DRA)'),
        'description' => t('Graphic Documentation')
      )
    );
  }
}
