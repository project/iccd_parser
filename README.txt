The ICCD Parser Module extends the Feeds Module implementing a parser for the ICCD xml files.

ICCD is a standard for Cultural Heritage Data in Italy.
For further information about how to use Feeds and configure and importer, and therefore use properly this parser,
check the Feeds Documentation:
http://drupal.org/node/622698
