<?php

/**
 * @file
 * Parser per gli xml dei Beni Culturali
 *
 */

/**
 * Parse the feed into a data structure.
 *
 * @param $feed
 *  The feed object (contains the URL or the parsed XML structure.
 * @return
 *  stdClass The structured datas extracted from the feed.
 */
function iccd_parser_parse($string) {
  if (!defined('LIBXML_VERSION') || (version_compare(phpversion(), '5.1.0', '<'))) {
    @ $xml = simplexml_load_string($string, NULL);
  }
  else {
    @ $xml = simplexml_load_string($string, NULL, LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NOCDATA);
  }
  // Got a malformed XML.
  if ($xml === FALSE || is_null($xml)) {
    return FALSE;
  }
  $feed_type = _parser_iccd_format_detect($xml);
  if ($feed_type ==  "archeomedgp") {
    return _parser_iccd_glcp_parse($xml);
  }
  if ($feed_type == "archeomedmc") {
    return _parser_iccd_mc_parse($xml);
  }
  if ($feed_type == "archeomedsi") {
    return _parser_iccd_si_parse($xml);
  }
  return FALSE;
}

/**
 * Get the cached version of the <var>$url</var>
 */
function _parser_iccd_cache_get($url) {
  $cache_file = _parser_iccd_sanitize_cache() .'/'. md5($url);
  if (file_exists($cache_file)) {
    $file_content = file_get_contents($cache_file);
    return unserialize($file_content);
  }
  return FALSE;
}

/**
 * Determine the xml format of a SimpleXML parsed object structure.
 *
 * @param $xml
 *  SimpleXML-preprocessed feed.
 * @return
 *  The feed format short description or FALSE if not compatible.
 */
function _parser_iccd_format_detect($xml) {
  if (!is_object($xml)) {
    return FALSE;
  }
  $type = drupal_strtolower($xml->getName());
  $normativa = drupal_strtolower($xml->csm_info->nome_normativa);
  if ($type == 'csm_root') {
    if ($normativa == 'archeomedgp') {
      return 'archeomedgp';
    }
    elseif ($normativa == 'archeomedmc') {
      return 'archeomedmc';
    }
    elseif ($normativa == 'archeomedsi') {
      return 'archeomedsi';
    }
    return FALSE;
  }
  return FALSE;

}

/**
 * Parse "Gestione Parco" xml.
 */
function _parser_iccd_glcp_parse($feed_XML) {
  $parsed_source = array();

  $base = (string) array_shift($feed_XML->xpath("@base"));
  if (!valid_url($base, TRUE)) {
    $base = FALSE;
  }

  // Detect the title
  $parsed_source['title'] = isset($feed_XML->csm_info->tipo) ? _parser_iccd_title("{$feed_XML->csm_info->tipo}") : "";

  if (valid_url($parsed_source['link']) && !valid_url($parsed_source['link'], TRUE) && !empty($base)) {
    $parsed_source['link'] = $base . $parsed_source['link'];
  }

  $parsed_source['items'] = array();

  foreach ($feed_XML->schede->scheda as $scheda) {
    $original_url = NULL;

    $guid = !empty($scheda->CD->ISK) ? "{$scheda->CD->ISK}" : $scheda->CD->ISK;

    $additional_taxonomies = array();

    $title = "{$scheda->CD->RSS->RSSN}";

    $body = $scheda->DE->DEG->DEGG;

    $item = array();
    $item['title'] = _parser_iccd_title($title, $body);
    $item['timestamp'] = _parser_iccd_parse_date($scheda->CD->ETS);
    $item['guid'] = $guid;
    $item['esc'] = $scheda->CD->ESC;
    $item['orgt'] = $scheda->OR->ORG->ORGT;
    $item['orgn'] = $scheda->OR->ORG->ORGN;
    $item['orgi'] = $scheda->OR->ORG->ORGI;
    $item['orga'] = $scheda->OR->ORG->ORGA;
    $item['orpd'] = $scheda->OR->ORP->ORPD;
    $item['orpt'] = $scheda->OR->ORP->ORPT;
    $item['orpq'] = $scheda->OR->ORP->ORPQ;
    $item['orpn'] = $scheda->OR->ORP->ORPN;
    $item['ored'] = $scheda->OR->ORE->ORED;
    $item['oret'] = $scheda->OR->ORE->ORET;
    $item['oreq'] = $scheda->OR->ORE->OREQ;
    $item['oref'] = $scheda->OR->ORE->OREF;
    $item['oren'] = $scheda->OR->ORE->OREN;
    $item['degg'] = $scheda->DE->DEG->DEGG;
    $item['degs'] = $scheda->DE->DEG->DEGS;
    $item['degv'] = $scheda->DE->DEG->DEGV;
    $item['degz'] = $scheda->DE->DEG->DEGZ;
    $item['dega'] = $scheda->DE->DEG->DEGA;
    $item['deaa'] = $scheda->DE->DEA->DEAA;
    $item['deac'] = $scheda->DE->DEA->DEAC;
    $item['deas'] = $scheda->DE->DEA->DEAS;
    $item['daci'] = $scheda->AS->DAC->DACI;
    $item['dacn'] = $scheda->AS->DAC->DACN;
    $item['dacp'] = $scheda->AS->DAC->DACP;
    $item['daca'] = $scheda->AS->DAC->DACA;
    $item['dacd'] = $scheda->AS->DAC->DACD;
    $item['dacf'] = $scheda->AS->DAC->DACF;
    $item['dacv'] = $scheda->AS->DAC->DACV;
    $item['dacm'] = $scheda->AS->DAC->DACM;
    $item['oapa'] = $scheda->AS->OAP->OAPA;
    $item['oapo'] = $scheda->AS->OAP->OAPO;
    $item['oapn'] = $scheda->AS->OAP->OAPN;
    $item['oapc'] = $scheda->AS->OAP->OAPC;
    $item['oapm'] = $scheda->AS->OAP->OAPM;
    $item['dspb'] = $scheda->DS->DSP->DSPB;
    $item['dspr'] = $scheda->DS->DSP->DSPR;
    $item['dspa'] = $scheda->DS->DSP->DSPA;
    $item['dsps'] = $scheda->DS->DSP->DSPS;
    $item['dsdv'] = $scheda->DS->DSD->DSDV;

    $parsed_source['items'][] = $item;
  }
  return $parsed_source;
}

/**
 * Parse "Sito" xml.
 */
function _parser_iccd_si_parse($feed_XML) {
  $parsed_source = array();

  $base = (string) array_shift($feed_XML->xpath("@base"));
  if (!valid_url($base, TRUE)) {
    $base = FALSE;
  }

  // Detect the title
  $parsed_source['title'] = isset($feed_XML->csm_info->tipo) ? _parser_iccd_title("{$feed_XML->csm_info->tipo}") : "";

  if (valid_url($parsed_source['link']) && !valid_url($parsed_source['link'], TRUE) && !empty($base)) {
    $parsed_source['link'] = $base . $parsed_source['link'];
  }

  $base_fta = $feed_XML->csm_info->percorsi->fta;
  $base_dra = $feed_XML->csm_info->percorsi->dra;

  $parsed_source['items'] = array();

  foreach ($feed_XML->schede->scheda as $scheda) {
    $original_url = NULL;

    $guid = !empty($scheda->CD->ISK) ? "{$scheda->CD->ISK}" : $scheda->CD->ISK;

    $additional_taxonomies = array();

    $title = "{$scheda->CD->RSS->RSSN}";

    $body = $scheda->DE->DEG->DEGG;

    $item = array();
    $item['title'] = _parser_iccd_title($title, $body);
    $item['timestamp'] = _parser_iccd_parse_date($scheda->CD->ETS);
    $item['guid'] = $guid;
    $item['esc'] = $scheda->CD->ESC;
    $item['ogtd'] = $scheda->OG->OGT->OGTD;
    $item['ogtt'] = $scheda->OG->OGT->OGTT;
    $item['ogtn'] = $scheda->OG->OGT->OGTN;
    $item['ogty'] = $scheda->OG->OGT->OGTY;
    $item['pvcs'] = $scheda->LC->PVC->PVCS;
    $item['pvcr'] = $scheda->LC->PVC->PVCR;
    $item['pvcp'] = $scheda->LC->PVC->PVCP;
    $item['pvcc'] = $scheda->LC->PVC->PVCC;
    $item['pvcl'] = $scheda->LC->PVC->PVCL;
    $item['pvci'] = $scheda->LC->PVC->PVCI;
    $item['dtzg'] = $scheda->DT->DTZ->DTZG;
    $item['dtm'] = $scheda->DT->DTM;
    $item['stcp'] = $scheda->CO->STC->STCP;
    $item['stcc'] = $scheda->CO->STC->STCC;
    $item['rstp'] = $scheda->RS->RST->RSTP;
    $item['rstt'] = $scheda->RS->RST->RSTT;
    $item['deso'] = $scheda->DA->DES->DESO;
    $item['nsc'] = $scheda->DA->NSC;
    $item['cdgg'] = $scheda->TU->CDG->CDGG;
    $item['cdgs'] = $scheda->TU->CDG->CDGS;
    $item['cdgi'] = $scheda->TU->CDG->CDGI;
    $item['nvct'] = $scheda->TU->NVC->NVCT;
    $item['nvce'] = $scheda->TU->NVC->NVCE;
    if ($scheda->DO->FTA) {
      foreach ($scheda->DO->FTA as $fta) {
        $item['fta'][] = $base_fta . '/' . $fta->FTAR . '/' . $fta->FTAI;
      }
    }

    if ($scheda->DO->DRA) {
      foreach ($scheda->DO->DRA as $dra) {
        $item['dra'][] = $base_dra . '/' . $dra->DRAY . '/' . $dra->DRAL;
      }
    }

    $parsed_source['items'][] = $item;
  }
  return $parsed_source;
}


/**
 * Parse "Monumento Complesso" xml.
 */
function _parser_iccd_mc_parse($feed_XML) {
  $parsed_source = array();

  $base = (string) array_shift($feed_XML->xpath("@base"));
  if (!valid_url($base, TRUE)) {
    $base = FALSE;
  }

  // Detect the title
  $parsed_source['title'] = isset($feed_XML->csm_info->tipo) ? _parser_iccd_title("{$feed_XML->csm_info->tipo}") : "";

  if (valid_url($parsed_source['link']) && !valid_url($parsed_source['link'], TRUE) && !empty($base)) {
    $parsed_source['link'] = $base . $parsed_source['link'];
  }

  $base_fta = $feed_XML->csm_info->percorsi->fta;
  $base_dra = $feed_XML->csm_info->percorsi->dra;

  $parsed_source['items'] = array();

  foreach ($feed_XML->schede->scheda as $scheda) {
    $original_url = NULL;

    $guid = !empty($scheda->CD->ISK) ? "{$scheda->CD->ISK}" : $scheda->CD->ISK;

    $additional_taxonomies = array();

    $title = "{$scheda->CD->RSS->RSSN}";

    $body = $scheda->DE->DEG->DEGG;

    $item = array();
    $item['title'] = _parser_iccd_title($title, $body);
    $item['timestamp'] = _parser_iccd_parse_date($scheda->CD->ETS);
    $item['guid'] = $guid;
    $item['esc'] = $scheda->CD->ESC;
    $item['ogtd'] = $scheda->OG->OGT->OGTD;
    $item['ogtc'] = $scheda->OG->OGT->OGTC;
    $item['ogtf'] = $scheda->OG->OGT->OGTF;
    $item['ogtn'] = $scheda->OG->OGT->OGTN;
    $item['pvcs'] = $scheda->LC->PVC->PVCS;
    $item['pvcr'] = $scheda->LC->PVC->PVCR;
    $item['pvcp'] = $scheda->LC->PVC->PVCP;
    $item['pvcc'] = $scheda->LC->PVC->PVCC;
    $item['pvcl'] = $scheda->LC->PVC->PVCL;
    $item['pvci'] = $scheda->LC->PVC->PVCI;
    $item['dtzg'] = $scheda->DT->DTZ->DTZG;
    $item['dtsi'] = $scheda->DT->DTS->DTSI;
    $item['dtm'] = $scheda->DT->DTM;
    $item['stcp'] = $scheda->CO->STC->STCP;
    $item['stcc'] = $scheda->CO->STC->STCC;
    $item['rstp'] = $scheda->RS->RST->RSTP;
    $item['rstt'] = $scheda->RS->RST->RSTT;
    $item['deso'] = $scheda->DA->DES->DESO;
    $item['fnsd'] = $scheda->DA->FNS->FNSD;
    $item['eled'] = $scheda->DA->ELE->ELED;
    $item['spad'] = $scheda->DA->SPA->SPAD;
    $item['cdgg'] = $scheda->TU->CDG->CDGG;
    $item['cdgs'] = $scheda->TU->CDG->CDGS;
    $item['cdgi'] = $scheda->TU->CDG->CDGI;
    $item['nvct'] = $scheda->TU->NVC->NVCT;
    $item['nvce'] = $scheda->TU->NVC->NVCE;
    if ($scheda->DO->FTA) {
      foreach ($scheda->DO->FTA as $fta) {
        $item['fta'][] = $base_fta . '/' . $fta->FTAR . '/' . $fta->FTAI;
      }
    }

    if ($scheda->DO->DRA) {
      foreach ($scheda->DO->DRA as $dra) {
        $item['dra'][] = $base_dra . '/' . $dra->DRAY . '/' . $dra->DRAL;
      }
    }

    $parsed_source['items'][] = $item;
  }
  return $parsed_source;
}

/**
 * Parse a date comes from a xml.
 *
 * @param $date_string
 *  The date string in various formats.
 * @return
 *  The timestamp of the string or the current time if can't be parsed
 */
function _parser_iccd_parse_date($date_str) {
  // PHP < 5.3 doesn't like the GMT- notation for parsing timezones.
  $date_str = str_replace("GMT-", "-", $date_str);
  $date_str = str_replace("GMT+", "+", $date_str);
  $parsed_date = strtotime($date_str);
  if ($parsed_date === FALSE || $parsed_date == -1) {
    $parsed_date = _parser_iccd_parse_w3cdtf($date_str);
  }
  return $parsed_date === FALSE ? time() : $parsed_date;
}

/**
 * Parse the W3C date/time format, a subset of ISO 8601.
 *
 * PHP date parsing functions do not handle this format.
 * See http://www.w3.org/TR/NOTE-datetime for more information.
 * Originally from MagpieRSS (http://magpierss.sourceforge.net/).
 *
 * @param $date_str
 *   A string with a potentially W3C DTF date.
 * @return
 *   A timestamp if parsed successfully or FALSE if not.
 */
function _parser_iccd_parse_w3cdtf($date_str) {
  if (preg_match('/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2})(:(\d{2}))?(?:([-+])(\d{2}):?(\d{2})|(Z))?/', $date_str, $match)) {
    list($year, $month, $day, $hours, $minutes, $seconds) = array($match[1], $match[2], $match[3], $match[4], $match[5], $match[6]);
    // Calculate the epoch for current date assuming GMT.
    $epoch = gmmktime($hours, $minutes, $seconds, $month, $day, $year);
    if ($match[10] != 'Z') { // Z is zulu time, aka GMT
      list($tz_mod, $tz_hour, $tz_min) = array($match[8], $match[9], $match[10]);
      // Zero out the variables.
      if (!$tz_hour) {
        $tz_hour = 0;
      }
      if (!$tz_min) {
        $tz_min = 0;
      }
      $offset_secs = (($tz_hour * 60) + $tz_min) * 60;
      // Is timezone ahead of GMT?  If yes, subtract offset.
      if ($tz_mod == '+') {
        $offset_secs *= -1;
      }
      $epoch += $offset_secs;
    }
    return $epoch;
  }
  else {
    return FALSE;
  }
}

/**
 * Prepare raw data to be a title
 */
function _parser_iccd_title($title, $body = FALSE) {
  if (empty($title) && !empty($body)) {
    // Explode to words and use the first 3 words.
    $words = preg_split("/[\s,]+/", strip_tags($body));
    $title = $words[0] .' '. $words[1] .' '. $words[2];
  }
  return $title;
}
